import { Assets, Container, Graphics, Rectangle, Sprite, Text } from "pixi.js";
import { CONFIG } from "./config";

export class ReelSymbol extends Container {
    bg: Graphics
    text: Text
    sprite: Sprite

    constructor(letter: string) {
        super()

        const sprite = new Sprite(Assets.get(CONFIG.symbol_texture[letter]))
        sprite.scale.set(100 / sprite.width)
        sprite.anchor.set(0.5)
        this.addChild(sprite)
        this.sprite = sprite

        const bg = new Graphics()
        bg.name = 'bg'
        bg.beginFill(0x10ffff)
        bg.drawRect(-50, -50, 100, 100)
        bg.endFill()
        bg.alpha = 0.2

        this.addChild(bg)
        this.bg = bg

        const frame = new Graphics()
        frame.name = 'frame'
        frame.lineStyle(2, 0x000010)
        frame.moveTo(-50, -50)
        frame.lineTo(50, -50)
        frame.lineTo(50, 50)
        frame.lineTo(-50, 50)
        frame.lineTo(-50, -50)
        frame.alpha = 1

        this.addChild(frame)

        const text = new Text(letter, {
            fontFamily: 'Arial',
            fontSize: 24,
            fill: 0xff1010,
            align: 'center',
            dropShadow: true,
            dropShadowBlur: 2,
            dropShadowDistance: 0,
            dropShadowColor: 0x000000
        })
        text.position.set(-text.width / 2, -text.height / 2)
        text.alpha = 0.9

        this.text = text
        this.addChild(text)
    }

    setLetter(letter: string) {
        this.sprite.texture = Assets.get(CONFIG.symbol_texture[letter])
        this.text.text = letter
        this.text.position.set(-this.text.width / 2, -this.text.height / 2)
    }

    getLetter() {
        return this.text.text
    }

    highlight(isOn: boolean) {
        const bg = this.bg
        bg.clear()
        bg.beginFill(isOn ? 0xff10ff : 0x10ffff)
        bg.drawRect(-50, -50, 100, 100)
        bg.endFill()
        bg.alpha = 0.2
    }
}