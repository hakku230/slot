import { Container, Ticker } from "pixi.js";
import { ReelSymbol } from "./Symbol";
import { callback } from "./helper";

const SYMBOL_POS_Y = [-200, -100, 0, 100, 200]

export class Reel extends Container {
    state: 'idle' | 'roll' | 'stop' | 'toStop'
    sequence: string[]
    symbols: ReelSymbol[]
    zero_index: number

    stopCallback: callback;
    stopIndex: number | false;

    constructor(DATA) {
        super()

        this.zero_index = 0
        this.symbols = []
        this.sequence = DATA.sequence

        for(let num = 0; num < 5; num++) {
            const symbol = new ReelSymbol(this.getSequenceSymbol(this.zero_index + num))
            symbol.position.y = SYMBOL_POS_Y[num]

            this.symbols.push(symbol)
            this.addChild(symbol)
        }

        this.state = 'idle'

        const ticker = new Ticker()

        ticker.add(this.process.bind(this))

        ticker.start()
    }

    getSequenceSymbol(num: number): string {
        if (num < 0) num = this.sequence.length + num;

        return this.sequence[num % this.sequence.length]
    }

    process(time: number) {
        if (this.state === 'roll' || this.state === 'toStop') {
            this.position.y += time * 20 // 10

            if ( this.position.y > 50 ) {
                if ( this.state === 'toStop' ) {
                    if ( this.stopIndex === false || this.stopIndex === this.zero_index ) this.state = 'stop'
                }

                this.position.y = -50
                this.zero_index = (this.zero_index - 1) % this.sequence.length
                this.symbols.forEach((s, num) => {
                    s.setLetter(this.getSequenceSymbol(this.zero_index + num))
                })
            }
        }

        if (this.state === 'stop') {
            this.position.y = Math.round(this.position.y)

            if ( this.position.y < 0 ) this.position.y += time * 6.66 // 3
            else if ( this.position.y > 0 ) this.position.y -= 1
            else {
                this.state = 'idle'
                this.stopCallback()
            }
        }
    }

    start() {
        this.state = 'roll'
    }

    async stop(testValue: string) {
        this.state = 'toStop'
        this.stopIndex = testValue ? (this.sequence.findIndex(symbol => symbol === testValue) - 1 - this.sequence.length) % this.sequence.length : false

        return new Promise(res => {
            this.stopCallback = res
        })
    }
}