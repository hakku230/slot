import { Assets, Container, Sprite } from "pixi.js";
import gsap from "gsap";

export class Prizes extends Container {
    items: Sprite[]
    repeats: number = 0

    constructor() {
        super()

        const items = []

        for (let i = 0; i < 10; i++) {
            const item = new Sprite(Assets.get('Icon31'))
            this.reset(item)
            this.addChild(item)
            items.push(item)
        }

        this.items = items
    }

    start(repeats: number = 10) {
        this.repeats = repeats

        this.items.forEach((item, num) => {
            if ( this.repeats > 0 ) this.launch(item, 0.1 * num)
        })
    }

    launch(item: Sprite, delay = 0) {
        this.repeats -= 1

        gsap.to(item, {
            x: item.destination.x * 0.9,
            y: item.destination.y,
            duration: 0.5,
            ease: "circ.out",
            delay: delay,
            onComplete: () => {
                gsap.to(item, {
                    x: item.destination.x,
                    y: 155,
                    duration: 0.5,
                    ease: "power1.in",
                    onComplete: () => {
                        this.reset(item)

                        if ( this.repeats > 0 ) this.launch(item)
                    }
                })
            }
        })
    }

    reset(item: Sprite) {
        item.position.set(Math.floor(Math.random() * 100) - 50, 155)
        item.destination = {
            x: (Math.floor(Math.random() * 530) - 280) / 2,
            y: -item.height / 2 + Math.floor(Math.random() * 100) - 50
        }
    }
}