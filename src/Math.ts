import { CONFIG } from "./config"
import { lineResult } from "./helper"

export class Mathematics {
    static calculate(map: string[][]) {
        let result: {
            lines: lineResult[],
            totalWin: number
        } = {
            lines: [],
            totalWin: 0
        }

        CONFIG.lines.forEach((line, lineId) => {
            let symbol = map[line[0]][0]
            let length = 0
            
            for (let col = 0; col < 5; col++) {
                if ( symbol === CONFIG.wild ) symbol = map[line[col]][col]
                if ( map[line[col]][col] === symbol || map[line[col]][col] === CONFIG.wild ) length++
                else break;
            }

            if ( length > 2 ) {
                if ( symbol === CONFIG.wild ) symbol = CONFIG.higher_symbol
                const win = CONFIG.win[symbol][length]

                result.lines.push({
                    line,
                    length,
                    symbol,
                    win
                })

                result.totalWin += win
            }
        })

        return result
    }
}