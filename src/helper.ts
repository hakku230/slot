export type callback = (...args: any[]) => any

export const sign = (value: number) => value < 0 ? -1 : 1

export type lineResult = {
    line: number[],
    length: number,
    symbol: string,
    win: number
}