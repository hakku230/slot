import { Application, Assets, utils } from "pixi.js";
import { Slot } from "./Slot";

const HUD = document.querySelector('#hud')
const SPIN_BTN = document.querySelector('button[type="spin"]')

document.querySelector('button[type="help"]').onclick = () => {
    document.querySelector('#help').style.top = '0'
}

document.querySelector('.help .close').onclick = () => {
    document.querySelector('#help').style.top = '-100vh'
}

const loadResources = async () => {
    const config = ['icons', 'items']

    for (let i = 0; i < config.length; i++) {
        const resName = config[i]

        utils.clearTextureCache()
        await Assets.load(resName + '.json');
    }
}

async function createGame() {
    await loadResources()

    const app = new Application({ resizeTo: window, backgroundAlpha: 0.5 });

    globalThis.__PIXI_APP__ = app;

    document.querySelector('#game').appendChild(app.view);

    return new Game(app)
}

export class Game {
    application: Application
    slot: Slot

    MAX_WIDTH: number
    MAX_HEIGHT: number

    timeout: number | undefined

    constructor(application: Application) {
        this.application = application
        this.slot = new Slot()

        this.application.stage.addChild(this.slot)

        SPIN_BTN.onpointerdown = () => {
            this.timeout = setTimeout(() => {
                SPIN_BTN.classList.contains('collapsed') ? SPIN_BTN.classList.remove('collapsed') : SPIN_BTN.classList.add('collapsed')
                this.resize()
            }, 1000)
        }

        SPIN_BTN.onpointerup = () => {
            clearTimeout(this.timeout)
        }

        this.MAX_WIDTH = this.slot.mask.width // 500
        this.MAX_HEIGHT = this.slot.mask.height // 300

        window.onresize = this.resize.bind(this)
        this.resize()
    }

    resize() {
        const MAX_WIDTH = this.MAX_WIDTH
        const MAX_HEIGHT = this.MAX_HEIGHT

        this.slot.position.set(window.innerWidth / 2, window.innerHeight / 2)
    
        if (window.innerHeight < MAX_HEIGHT || window.innerWidth < MAX_WIDTH) this.slot.scale.set( Math.min(window.innerHeight / MAX_HEIGHT, window.innerWidth / MAX_WIDTH) )
    
        HUD.style.bottom = window.innerHeight > 370 ? 'calc(50% - ' + ( MAX_HEIGHT * this.slot.scale.y / 2 + HUD.scrollHeight ) + 'px)' : '0px'
    }
}

const game = createGame()