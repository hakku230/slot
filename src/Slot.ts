import { Container, Graphics, Text, Ticker } from "pixi.js";
import { Reel } from "./Reel";
import { Mathematics } from "./Math";
import { CONFIG } from "./config";
import { lineResult } from "./helper";
import { Prizes } from "./Prizes";

const SPIN_BTN = document.querySelector('button[type="spin"]')
const FASTSPIN_BTN = document.querySelector('#fastspin')
const AUTOSPIN_BTN = document.querySelector('#autospin')
const BALANCE = document.querySelector('.balance .value')

export class Slot extends Container {
    state: 'idle' | 'rolling' | 'stopping' | 'result'
    reels: Reel[]
    balance: number
    overlay: Graphics;
    winText: Text;
    prizes: Prizes;

    constructor() {
        super()

        this.reels = []

        CONFIG.reels.forEach((REEL_DATA, num) => {
            const reel = new Reel(REEL_DATA)
            reel.position.set(REEL_DATA.pos.x, REEL_DATA.pos.y)

            this.reels.push(reel)
            this.addChild(reel)
        })

        this.prizes = new Prizes()
        this.addChild(this.prizes)

        const overlay = new Graphics()
        overlay.beginFill(0x000000)
        overlay.drawRect(-250, -150, 500, 300)
        overlay.endFill()
        overlay.alpha = 0.5
        overlay.visible = false
        this.overlay = overlay
        this.addChild(overlay)

        const winText = new Text(0, {
            fontFamily: 'Arial',
            fontSize: 56,
            fill: 0xff10ff,
            align: 'center',
            dropShadow: true
        })
        winText.visible = false
        this.winText = winText
        this.addChild(winText)

        const mask = new Graphics()
        mask.beginFill(0xffffff)
        mask.drawRect(-251, -150, 500, 300)
        mask.endFill()

        this.mask = mask

        this.addChild(mask)

        this.state = 'idle'

        this.balance = 1000
        this.updateBalance()

        window.slot = this

        SPIN_BTN.onclick = (e) => {
            if ( e.target !== SPIN_BTN ) return false;
            
            this.spin()
        }

        AUTOSPIN_BTN.onclick = (e) => {
            if ( e.target !== AUTOSPIN_BTN ) return false;
            
            AUTOSPIN_BTN.checked ? ticker.start() : ticker.stop()
        }

        const ticker = new Ticker()

        ticker.add(this.autospin.bind(this))
    }

    spin() {
        if ( this.state === 'idle' && this.balance >= 10 ) {
            SPIN_BTN.disabled = true

            this.balance -= 10
            this.updateBalance()

            this.startRoll()

            setTimeout(async () => {
                await this.stopRoll()
            }, FASTSPIN_BTN.checked ? 100 : 1000)
        }
    }

    autospin() {
        this.spin()
    }

    async showTotalWin(win: string | number) {
        return new Promise(res => {
            this.winText.text = win
            this.winText.position.set(- this.winText.width / 2, - this.winText.height / 2)
            this.overlay.visible = true
            this.winText.visible = true

            const durationMultiplier = Math.ceil(win / 40)

            this.prizes.start(6 + 10 * (durationMultiplier - 1))

            setTimeout(() => {
                this.overlay.visible = false
                this.winText.visible = false

                res(true)
            }, 1000 * durationMultiplier)
        })
    }

    updateBalance() {
        BALANCE.innerHTML = this.balance
    }
    
    startRoll() {
        this.state = 'rolling'
        this.reels.forEach(r => r.start())
    }

    async stopRoll() {
        this.state = 'stopping'

        for (let num = 0; num < this.reels.length; num++) {
            const testValue = document.querySelector('#test-' + (num + 1)).value
            await this.reels[num].stop(testValue)
        }

        this.showResult()
    }

    async showResult() {
        this.state = 'result'

        const result = Mathematics.calculate(this.getMap())

        console.log(result)

        for (let i = 0; i < result.lines.length; i++) {
            await this.showWin(result.lines[i])
        }

        if ( result.totalWin > 0 ) await this.showTotalWin(result.totalWin)

        this.setIdle()
    }

    showWin(result: lineResult) {
        console.log('showWin', result)
        return new Promise(res => {
            result.line.forEach((row, col) => {
                if (col < result.length) this.reels[col].symbols[row + 1].highlight(true)
            })

            setTimeout(() => {
                result.line.forEach((row, col) => {
                    if (col < result.length) this.reels[col].symbols[row + 1].highlight(false)
                })

                this.balance += result.win
                this.updateBalance()

                res(true)
            }, 500)
        })
    }

    setIdle() {
        this.state = 'idle'
        SPIN_BTN.disabled = false
    }

    getMap(): string[][] {
        const map: string[][] = [ [], [], [] ]

        this.reels.forEach((r, col) => {
            r.symbols.forEach((s, row) => {
                if ( row === 0 || row === 4 ) return;

                map[row - 1][col] = s.getLetter()
            })
        })

        return map
    }
}